import { Observable } from 'rxjs'; //reactive x

interface Account {
  accountId: string;
  accountNumber: string;
  bankId: string;
  bankName: string;
  ownerName: string;
  createdAt: string;
}

export interface RegisterPixKeyFindRpcResponse {
  id: string;
  kind: string;
  key: string;
  account: Account;
  createdAt: string;
}

export interface RegisterPixKeyRpcResp {
  id: string;
  status: string;
  error: string;
}

export interface RegisterPixKeyData {
  kind: string;
  key: string;
  accountId: string;
}

export interface PixKeyFindData {
  kind: string;
  key: string;
}

export interface PixKeyClientGrpc {
  registerPixKey: (
    data: RegisterPixKeyData,
  ) => Observable<RegisterPixKeyRpcResp>;
  find: (data: PixKeyFindData) => Observable<RegisterPixKeyFindRpcResponse>;
}
