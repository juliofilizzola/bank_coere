import { IsString } from 'class-validator';

export class CreateBankAccountDto {
  @IsString()
  account_name: string;

  @IsString()
  owner_name: string;
}
