import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { PixKeyKind } from '../../pix-keys/entities/pix-key.entity';

export class CreateTransactionDto {
  @IsString({
    message: 'pix_key_key has be string',
  })
  @IsNotEmpty({
    message: 'pix_key_key is required',
  })
  pix_key_key: string;

  @IsEnum(PixKeyKind)
  @IsNotEmpty({
    message: 'pix_key_kind is required',
  })
  pix_key_kind: PixKeyKind;

  @IsString({
    message: 'description has be string',
  })
  @IsOptional()
  description?: string;

  @IsNumber(
    {
      maxDecimalPlaces: 2,
    },
    {
      message: 'amount has be number',
    },
  )
  @Min(0.01)
  @IsNotEmpty({
    message: 'amount is required',
  })
  amount: number;
}
