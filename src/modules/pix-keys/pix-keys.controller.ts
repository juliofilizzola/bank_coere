import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { PixKeysService } from './pix-keys.service';
import { CreatePixKeyDto } from './dto/create-pix-key.dto';

@Controller('pix-keys')
export class PixKeysController {
  constructor(private readonly pixKeysService: PixKeysService) {}

  @Post('/:bank_account')
  create(
    @Body() createPixKeyDto: CreatePixKeyDto,
    @Param('bank_account') bank_account: string,
  ) {
    return this.pixKeysService.create(createPixKeyDto, bank_account);
  }

  @Get(':bank_account')
  findAll(@Param('bank_account') bank_account: string) {
    return this.pixKeysService.findAll(bank_account);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.pixKeysService.findOne(id);
  }
}
