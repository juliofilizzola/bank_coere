import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  OnModuleInit,
} from '@nestjs/common';
import { CreatePixKeyDto } from './dto/create-pix-key.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PixKey, PixKeyKind } from './entities/pix-key.entity';
import { BankAccount } from '../bank-accounts/entities/bank-account.entity';
import { ClientGrpc } from '@nestjs/microservices';
import {
  PixKeyClientGrpc,
  PixKeyFindData,
  RegisterPixKeyFindRpcResponse,
  RegisterPixKeyRpcResp,
} from '../../gRPC/interface/grpc.interface';
import { lastValueFrom } from 'rxjs';
import { PixKeyGrpcUnknownError } from '../../errors/PixKeyGrpcUnknown.error';

@Injectable()
export class PixKeysService implements OnModuleInit {
  private pixGrpcService: PixKeyClientGrpc;

  constructor(
    @InjectRepository(PixKey) private readonly repository: Repository<PixKey>,
    @InjectRepository(BankAccount)
    private readonly repositoryBank: Repository<BankAccount>,
    @Inject('PIX_PACKAGE')
    private readonly pixGrpc: ClientGrpc,
  ) {}

  onModuleInit(): void {
    this.pixGrpcService = this.pixGrpc.getService('PixService');
  }

  async create(
    createPixKeyDto: CreatePixKeyDto,
    bank_account: string,
  ): Promise<PixKey> {
    try {
      await this.repositoryBank.findOneOrFail({
        where: {
          id: bank_account,
        },
      });
    } catch (e) {
      throw new NotFoundException();
    }
    const pixKeyGRPC = await this.findRemotePixKey(createPixKeyDto);

    if (!pixKeyGRPC) {
      await this.createRemotePixKey({
        ...createPixKeyDto,
        accountId: bank_account,
      });
      return this.repository.save({
        bank_account: {
          id: bank_account,
        },
        ...createPixKeyDto,
      });
    } else {
      return this.createIfNotExists(bank_account, pixKeyGRPC);
    }
  }

  async findAll(bank_account_id: string): Promise<PixKey[]> {
    return this.repository.find({
      where: {
        bank_account_id: bank_account_id,
      },
    });
  }

  async findOne(id: string): Promise<PixKey> {
    return this.repository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  private async createIfNotExists(
    bankAccountId: string,
    remotePixKey: RegisterPixKeyFindRpcResponse,
  ): Promise<PixKey> {
    const hasLocalPixKey = await this.repository.exists({
      where: {
        key: remotePixKey.key,
      },
    });
    if (hasLocalPixKey) {
      throw new BadRequestException('Pix Key already exists');
    } else {
      return this.repository.save({
        id: remotePixKey.id,
        bank_account_id: bankAccountId,
        key: remotePixKey.key,
        kind: remotePixKey.kind as PixKeyKind,
      });
    }
  }
  private async createRemotePixKey(data: {
    kind: string;
    key: string;
    accountId: string;
  }): Promise<RegisterPixKeyRpcResp> {
    try {
      return await lastValueFrom(this.pixGrpcService.registerPixKey(data));
    } catch (e) {
      console.error(JSON.stringify(e));
      throw new PixKeyGrpcUnknownError(e.details);
    }
  }

  private async findRemotePixKey(
    data: PixKeyFindData,
  ): Promise<RegisterPixKeyFindRpcResponse | null> {
    try {
      return await lastValueFrom(this.pixGrpcService.find(data));
    } catch (e) {
      console.error(JSON.stringify(e));
      if (e.details === 'no key was found') {
        return null;
      }
      throw new PixKeyGrpcUnknownError(e.details);
    }
  }
}
