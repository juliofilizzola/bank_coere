import { PixKeyKind } from '../entities/pix-key.entity';
import { IsEnum, IsString } from 'class-validator';

export class CreatePixKeyDto {
  @IsString()
  key: string;

  @IsEnum(PixKeyKind)
  kind: PixKeyKind;
}
