import { Module } from '@nestjs/common';
import { BankAccountsModule } from './modules/bank-accounts/bank-accounts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BankAccount } from './modules/bank-accounts/entities/bank-account.entity';
import { PixKeysModule } from './modules/pix-keys/pix-keys.module';
import { PixKey } from './modules/pix-keys/entities/pix-key.entity';
import { ConfigModule } from '@nestjs/config';
import { TransactionModule } from './modules/transaction/transaction.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'db',
      database: 'bank_core',
      username: 'postgres',
      password: '123456',
      entities: [BankAccount, PixKey],
      synchronize: true,
    }),
    BankAccountsModule,
    PixKeysModule,
    TransactionModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
